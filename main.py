import numpy as np
from time import sleep, time
from Keithley6517B import Keithley6517B
from Keithley2657 import Keithley2657
from DataHandler import DataHandler
from LiveMonitor import LiveMonitor



### CONFIG ###
diamond_name = 'None'
pad_number = 0

vmin = 0
vmax = 30
vstep = 25
settling_wait = 1
read_interval = 1 #second, one reading takes about 2s (acquires 10 data points internally and averages them)
readings = 5
iterations = 1
### end CONFIG ###

comments = 'vmin=' + str(vmin) + '\n vmax=' + str(vmax) + '\n vstep=' + str(vstep) \
            + '\n settling_wait=' + str(settling_wait) + '\n read_interval=' + str(read_interval) \
            + '\n readings=' + str(readings) + '\n interations=' + str(iterations) 


class MeasureIV(object):
    def __init__(self):
        self.cfg6517 = 'config/keithley6517B.cfg'
        self.cfg2657 = 'config/keithley2657.cfg'
        self.am = None
        self.hv = None
        self.lm = LiveMonitor()
        self.dh = DataHandler()

        self.dh.createFile(diamond_name, pad_number, comments)
        self.done = False

    def get_col(self, arr, col):
        return map(lambda x : x[col], arr)
 
    def start(self):
        #hv source
        self.hv = Keithley2657(self.cfg2657)
        self.hv.open_instrument()
        self.hv.init_keithley()

        #ammeter
        self.am = Keithley6517B(self.cfg6517)
        self.am.open_serial_port()
        #self.am.init_keithley()

        #scan steps
        ssteps = np.arange(vmin, vmax+1, vstep)
        self.hv.set_output(True)
        sleep(settling_wait)
    
        terminated = False
        #self.am.read_iv() #remove first reading which always seem to be trash
        t0 = time()
        for s, step in enumerate(ssteps[1::]): #don't do zero twice
            
            if terminated:
                break
            
            results = []
            for r in range(readings):
                print 'Reading ' + str(r) + '/' + str(readings) + ' of ' + str(s) + '/' + str(len(ssteps))
                #data =  self.am.read_iv()
                data = {'current':0.00000000002, 'rest':0.01}
                volt = step
                curr = 10e12*data['current'] #current in pA
                compl = data['rest'] #1 for compliance
                t = time() - t0
                results.append([int(r), float(t), float(volt), float(curr), int(compl)])

                self.lm.setData(t, volt, curr, compl)
                self.lm.updatePlots()

                #check for compliance:
                pres = np.mean(results, axis=0)
                if pres[3] > 100000: #100nA
                    print 'Pooossey! Compliance! Terminating, terminating!'
                    terminated = True
                    break
                sleep(read_interval)


            if not terminated:
                self.dh.addIVData(results)
                print 'Scan point ' + str(s) + '/' + str(len(ssteps-1)) + ': U=' + str(round(pres[2],1)) + 'V, I=' + str(round(pres[3],4)) + 'pA'
                self.hv.set_bias(step)
                sleep(settling_wait)
            else:
                break


        self.hv.set_bias(0)
        self.hv.set_output(False)
        self.hv.close_instrument()
        self.dh.closeFile()
        self.am.close_serial_port()
        self.done = True


    def exit_gracefully(self):
        if not self.done:
            self.hv.set_bias(0)
            self.hv.set_output(False)
            self.hv.close_instrument()
            self.dh.closeFile()
            self.am.close_serial_port()


if __name__ == '__main__':
    iv = MeasureIV()
    try:
        for i in range(iterations):
            iv.start()
    except KeyboardInterrupt:
        pass
    finally:
        iv.exit_gracefully()