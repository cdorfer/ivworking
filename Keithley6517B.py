from ConfigParser import ConfigParser
import serial
from time import time, sleep
from string import maketrans
from collections import deque


class Keithley6517B():
    def __init__(self, config):
        self.cfg = self.read_config(config)
        self.name = self.cfg.get('DEVICE', 'name')
        self.serial_port = self.cfg.get('DEVICE', 'serial_port')
        self.compliance = float(self.cfg.get('DEVICE', 'compliance'))
        self.current_range = float(self.cfg.get('DEVICE', 'current_range'))
        self.bias_limit_pm = float(self.cfg.get('DEVICE', 'bias_limit_pm'))
        self.serial = None
        self.bOpen = False
        self.commandEndCharacter = chr(13) + chr(10)
        self.writeSleepTime = 0.1
        self.readSleepTime = 0.2

    def read_config(self, cfile):
        config = ConfigParser()
        config.read(cfile)
        return config

    def get_model_name(self):
        answ = self.get_answer_for_query('*IDN?')
        return answ

    def open_serial_port(self):
        try:
            self.serial = serial.Serial(
                port=self.serial_port,
                baudrate=57600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1, )
            print 'Openened serial port: \'%s\'' % self.serial_port + ' to:' + self.get_model_name()
            self.bOpen = True
        except:
            print 'Could not open serial Port: \'%s\'' % self.serial_port + ' to ' + self.name
            self.bOpen = False
        model = self.get_model_name()
        print model

    def close_serial_port(self):
        self.reset()
        self.serial.close()
        print 'Serial port closed!'


    def init_keithley(self, hot_start=False):
        sleep(self.writeSleepTime)
        self.reset()
        self.clear_status()
        self.clear_buffer()

        #setup for measurement
        self.write(':SYSTem:ZCHeck ON') #enable zero check
        #self.write(':SYSTem:ZCHeck OFF') #enable zero check
        #self.write(':SYSTem:ZCORrect OFF') #perform zero correction on future measurements
        
        self.write(':SENSe1:FUNCtion "CURRent:DC"')
        self.write(':SENSe1:CURRent:DC:AVERage:TYPE SCALar') #we use the simple averaging scalar filter
        self.write(':SENSe1:CURRent:DC:AVERage:COUNt 10') #average 10 readings
        self.write(':SENSe1:CURRent:DC:AVERage:TCONtrol REPeat') #take readings - average - flush
        self.write(':SENSe1:CURRent:DC:AVERage:STATe 1') #turn digital filter on

        self.write(':SENSe1:CURRent:DC:MEDian:STATe 1')
        self.write(':SENSe1:CURRent:DC:MEDian:RANK 5') #take median of 11 readings
        self.write(':SENSe1:CURRent:DC:NPLCycles 5') #reading speed: 100 ms per reading
        self.write(':SENSe1:CURRent:DC:RANGe:AUTO 1')


        self.write(':FORMat:ELEMents VSOurce,READing') #what we're reading out (,UNITs)

        self.write(':SOUR:VOLT:RANG 1000') #set voltage range to 1000
        self.write(':DISPlay:ENABle 1')

        self.write(':SYSTem:ZCHeck OFF') #disable zero check

        sleep(1)

        self.check_for_errors()
        print '\033[94m'
        print '### READBACK: ###'
        print 'Zero baseline correction: ', self.get_answer_for_query(':SYSTem:ZCORrect?')
        print 'Data is read as: ', self.get_answer_for_query(':FORMat:DATA?')
        print 'Measurement function: ', self.get_answer_for_query('SENS:FUNCtion?')
        print 'Digital filter type: ', self.get_answer_for_query(':SENSe1:CURRent:DC:AVERage:TYPE?')
        print 'Number of digital filter readings: ', self.get_answer_for_query(':SENSe1:CURRent:DC:AVERage:COUNt?')
        print 'Digital filter mode: ', self.get_answer_for_query(':SENSe1:CURRent:DC:AVERage:TCONtrol?')
        print 'Digital filter state: ', self.get_answer_for_query(':SENSe1:CURRent:DC:AVERage:STATe?')
        print 'Median filter state: ', self.get_answer_for_query(':SENSe1:CURRent:DC:MEDian:STATe?')
        print 'Median filter rank: ', self.get_answer_for_query(':SENSe1:CURRent:DC:MEDian:RANK?')
        print 'Data taking speed: ', self.get_answer_for_query(':SENSe1:CURRent:DC:NPLCycles?')
        print 'Voltage range: ', self.get_answer_for_query(':SOUR:VOLT:RANG?')
        print '#################'
        print '\033[0m'
        self.check_for_errors()

    def set_output(self, status):
        if status:
            self.write(':OUTPut ON')
        else:
            self.write(':OUTPut OFF')
        sleep(self.readSleepTime)
        print 'Output: ', self.get_answer_for_query(':OUTPut:STATe?')


    def read_iv(self):
        self.check_for_errors()
        #An overflow and out-of-limit read as +9.9E37; zero-check reads as +9.91E37; underflow reads as 0.00E00
        answer = self.get_answer_for_query(':READ?', 20)
        #print answer
        answer = answer.split()
        if len(answer) > 1:
            voltage = float(answer[1])
            current = float(answer[0])
            if len(answer) > 2:
                rest = answer[2]
            else:
                rest = 0
            return {'current': current, 'voltage': voltage, 'rest': rest}
        return None

    def check_for_errors(self):
        answ = self.get_answer_for_query('*ESR?')
        if self.is_number(answ):
            err = int(answ)
            binerr = bin(err)
            bits = [int(e) for e in binerr[2:]]
            bits = bits[::-1]

            err_codes = ['Operation Complete', '', 'Query Error', 'Device-dependent Error', 'Execution Error', 'Command Error', 'User Request', 'Power On']
            for i in range(len(bits)):
                if bits[i] == 1:
                    print '\033[91m' + err_codes[i] + '\033[0m'
            self.clear_status()


    def set_bias(self, voltage):
        cmd = ':SOURce:VOLTage ' + str(voltage)
        self.write(cmd)
        sleep(self.readSleepTime)
        self.check_for_errors()
        #print 'Voltage set to: ', round(float(self.get_answer_for_query(':SOURce:VOLTage?')),2)

    def reset(self):
        return self.write('*RST')

    def clear_status(self):
        return self.write('*CLS')

    def clear_buffer(self):
        if self.bOpen:
            while self.serial.inWaiting():
                self.read()
                sleep(self.readSleepTime)
        else:
            pass
        return self.write(':TRAC:CLEAR')


    def get_output_status(self):
        answer = self.get_answer_for_query(':OUTP?')
        while len(answer) > 1 and not self.is_number(answer):
            answer = self.get_answer_for_query(':OUTP?')
        if len(answer) > 0 and not answer == '':
            if answer.isdigit():
                return int(answer)
        else:
            return -1

    def get_trigger_count(self):
        data = self.get_answer_for_query(':TRIG:COUN?')
        if data == '':
            return -1
        return data if 0 <= int(data) <= 2500 else -1


    def get_answer_for_query(self, data, minlength=1):
        self.write(data)
        sleep(self.readSleepTime)
        data = self.read(minlength)
        return self.clear_string(data)

    def write(self, data):
        data += self.commandEndCharacter
        if self.bOpen:
            output = self.serial.write(data)
        else:
            output = True
        sleep(self.writeSleepTime)
        return output == len(data)

    def read(self, min_lenght=0):
        out = ''
        if not self.bOpen:
            return ''
        ts = time()
        max_time = 300
        k = 0
        while True:
            while self.serial.inWaiting() > 0 and time() - ts < max_time and not out.endswith(self.commandEndCharacter):
                out += self.serial.read(1)
                k += 1
            if out.endswith(self.commandEndCharacter):
                break
            if time() - ts > max_time:
                break
            if 0 < min_lenght <= len(out):
                break
            sleep(self.readSleepTime)
        if time() - ts > max_time:
            print "Tried reading for %s seconds." % (time() - ts), out
            try:
                print ord(out[-2]), ord(out[-1]), ord(self.commandEndCharacter[0]), ord(self.commandEndCharacter[1])
            except IndexError:
                print "Error trying: 'print ord(out[-2]),ord(out[-1])," \
                      "ord(self.commandEndCharacter[0]),ord(self.commandEndCharacter[1]),len(out)'"
            return ''
        #print 'received after %s tries: %s' % (k, out)
        return out

    def is_number(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    @staticmethod
    def clear_string(data):
        data = data.translate(None, '\r\n\x00\x13\x11\x10')
        data = data.translate(maketrans(',', ' '))
        return data.strip()


