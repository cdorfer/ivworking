####################################               
# Author: Christian Dorfer
# Email: cdorfer@phys.ethz.ch                                  
####################################

import datetime
from thread import start_new_thread
import numpy as np
import h5py


class DataHandler(object):     
    def __init__(self):
        self.hdf = None
        self.ivdata = None
        self.spcount = 1
        self.runnumber = self.readRunNumber()+1
        #self.livemon = livemon


    def createFile(self, diamond_name, pad_number, params, comment):
        
        #reset old one
        self.hdf= None
        self.ivdata = None
        self.spcount = 0
        
        #read and increase run number
        self.runnumber = self.increaseRunNumber()
        print 'Run number:', self.runnumber
         
        #create new h5py file
        fname = 'data/run' + str(self.runnumber) + ".hdf5"
        self.hdf = h5py.File(fname, "w", libver='latest')
        self.hdf.attrs['timestamp'] = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
        self.ivdata = self.hdf.create_group("ivdata")
        self.ivdata.attrs['diamond_name'] = diamond_name
        self.ivdata.attrs['pad_number'] = pad_number
        self.ivdata.attrs['comments'] = comment
        self.ivdata.attrs['vmin'] = params[0]
        self.ivdata.attrs['vmax'] = params[1]
        self.ivdata.attrs['vstep'] = params[2]
        self.ivdata.attrs['settling_wait'] = params[3]
        self.ivdata.attrs['read_interval'] = params[4]
        self.ivdata.attrs['readings'] = params[5]

        print('File ', fname, ' created.')
        
    
    def addIVData(self, results):
        sp = str(self.spcount)
        arr = np.array(results)
        self.ivdata.create_dataset(sp, data=arr, compression="gzip")
        self.spcount += 1

        #self.livemon.setIV(timestamp, voltage, current, compliance)
        #start_new_thread(self.livemon.updatePlots, ())
        #return sp
        
    
    def increaseRunNumber(self):
        with open('data/runnumber.dat', "r+") as f:
            runnumber = int(f.readline())
            f.seek(0)
            f.write(str(runnumber+1))
            return (runnumber+1)
            
    def readRunNumber(self):
        with open('data/runnumber.dat', "r") as f:
            return int(f.readline())

        
    def closeFile(self):
        self.hdf.flush()
        self.hdf.close()
        print('File for run ', str(self.runnumber), ' closed.')
    