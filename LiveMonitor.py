####################################              
# Author: Christian Dorfer
# Email: cdorfer@phys.ethz.ch                                  
####################################

import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt

class LiveMonitor(object):
    def __init__(self):
        #here we hold data:
        self.t = []
        self.v = []
        self.i = []
        self.compl = []

        plt.ion()
        fig, ax1 = plt.subplots(1)
        self.fig = fig
        self.ax1 = ax1

        self.ax1.set_title('Live Measurement')
        self.ax1.set_xlabel('Time [s]')
        self.ax1.set_ylabel('Voltage [V]', color='b')
        self.ax1.tick_params('y', colors='b')
        
        self.ax2 = self.ax1.twinx()
        self.ax2.set_ylabel('Leakage Current [pA]', color='r')
        self.ax2.tick_params('y', colors ='r')

        self.fig.canvas.draw()



    def setData(self, time, voltage, current, compliance):
        self.t.append(time)
        self.v.append(voltage)
        self.i.append(current)
        self.compl.append(compliance)

        
    def updatePlots(self): 
        self.ax1.plot(self.t[-2:], self.v[-2:], 'b-')
        self.ax2.plot(self.t[-1], self.i[-1], 'r+')
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    
    def closePlots(self):
        try:
            self.fig.clf()
            plt.close()
            del self.t, self.v, self.i, self.compl, self.fig, self.ax1   
        except:
            pass    
