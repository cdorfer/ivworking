import numpy as np
from time import sleep, time
import datetime
from Keithley6517B import Keithley6517B
from Keithley2657 import Keithley2657
from DataHandler import DataHandler
from LiveMonitor import LiveMonitor




class MeasureIV(object):
    def __init__(self, params):
        #hv source
        self.cfg2657 = 'config/keithley2657.cfg'
        self.hv = Keithley2657(self.cfg2657)
        self.hv.open_instrument()
        self.hv.init_keithley()

        #ammeter
        self.cfg6517 = 'config/keithley6517B.cfg'
        self.am = Keithley6517B(self.cfg6517)
        self.am.open_serial_port()
        self.am.init_keithley()

        #monitoring and data handling
        self.lm = LiveMonitor()
        self.dh = DataHandler()
        self.done = False

        self.soft_limit = 100000 #100nA
        self.hard_limit = 199000 #199nA

        ### CONFIG ###
        self.diamond_name = params[0]
        self.pad_number = params[1]
        self.vmin = params[2] #start voltage scan here
        self.vmax = params[3] #end voltage scan here
        self.vstep = params[4] #step size
        self.settling_wait = params[5] #after increasing the voltage wait this long
        self.read_interval = params[6] #inbetween current measurements wait this long
        self.readings = params[7] #number of current measurements for one voltage point
        self.sleeping = params[8] #sleep this long after an IV curve
        ### end CONFIG ###


        params = [self.vmin, self.vmax, self.vstep, self.settling_wait, self.read_interval, self.readings]

        comments = '\nvmin=' + str(self.vmin) + '\n vmax=' + str(self.vmax) + '\n vstep=' + str(self.vstep) \
            + '\n settling_wait=' + str(self.settling_wait) + '\n read_interval=' + str(self.read_interval) \
            + '\n readings=' + str(self.readings)

        self.dh.createFile(self.diamond_name, self.pad_number, params, comments)


    def readSMU(self):
        data =  self.am.read_iv()
        if data:
            curr = 10e11*data['current'] #current in pA
            compl = data['rest'] #1 for compliance (above 200nA)
            t = time()
            return [curr, compl, t]
        else:
            print 'Reading again.'
            self.readSMU()

    def checkCompliance(self, limit):
        [curr, compl, _] = self.readSMU()
        if compl == 1 or abs(float(curr)) > abs(limit):
            [curr, compl, _] = self.readSMU()
            if abs(float(curr)) > abs(limit): #check again to avoid one timers to end the measurement
                print 'Current = ' + str(curr) + 'nA'
                return True
        return False

    def quickCheckCompliance(self, curr, limit): #just to make things faster
        if abs(curr) > abs(limit):
            return self.checkCompliance(limit)
        return False

 
    def measureIV(self, hysteresis=False):
        self.hv.set_bias(0)
        self.hv.set_output(True)
        for i in range(5):    
            _ = self.am.read_iv() #do some readings (sometimes there's a few weird first points)
        sleep(self.settling_wait)

        if not hysteresis:
            self.hard_limit = self.soft_limit

        terminated = False
        compliance = False
        decrease_voltage = False
        t0 = time()

        volt = self.vmin

        while abs(volt) <= abs(self.vmax) and abs(volt) >= abs(self.vmin):
            if terminated:
                break
            
            results = []
            for r in range(self.readings):
                [curr, compl, t] = self.readSMU()
                results.append([int(r), float(t), float(volt), float(curr), int(compl)])
                print 'Reading ' + str(r) + '/' + str(self.readings) + ' at voltage: ' + str(volt) + 'V, Current = ' + str(round(curr,1)) + 'pA'
                self.lm.setData(t-t0, volt, curr, compl)
                self.lm.updatePlots()


                if self.quickCheckCompliance(curr, self.hard_limit):
                    print 'Reached hard current limit. Terminating.'      
                    terminated = True
                    break

                if hysteresis and self.quickCheckCompliance(curr, self.soft_limit):
                    print 'Reached soft current limit (' + str(self.soft_limit/1000) + 'nA), trying to go back in voltage now.'
                    
                    #try to come back to something below the soft limit
                    set_volt = 0
                    while self.checkCompliance(self.soft_limit):
                        compliance = True
                        set_volt = volt - self.vstep
                        self.hv.set_bias(set_volt)
                        sleep(4)
                        print 'Soft compliance! Setting ' + str(set_volt) + ' V'
                    volt = set_volt
                    decrease_voltage = False
                    break

                sleep(self.read_interval)

            #what follows is a masterpiece of logic:
            if compliance and decrease_voltage:
                volt = volt - self.vstep
            elif not compliance:
                volt = volt + self.vstep
            elif compliance:
                decrease_voltage = True
            else:
                print 'Help, I do not know what to do!'
                terminated = True

                
            self.dh.addIVData(results)
            if not terminated:
                self.hv.set_bias(volt)
                dtnow = datetime.datetime.now().strftime("%H:%M:%S")
                print dtnow + ": Sleep for " + str(self.settling_wait) + "s"
                sleep(self.settling_wait)
            else:
                break

        self.exit_gracefully()
        sleep(self.sleeping)



    def exit_gracefully(self):
        if not self.done:
            self.hv.set_bias(0)
            self.hv.set_output(False)
            self.hv.close_instrument()
            self.dh.closeFile()
            self.am.close_serial_port()
            self.done = True
            self.lm.closePlots()


def main():
    # diamond name - pad - start voltage - end voltage - step - settling wait - wait between current reads - # of current reads - sleep time after scan
    measure_params = [['Einstein', 1, 0, -3000, -25, 20, 1, 6, 1200],
                      ['Einstein', 1, 0,  3000,  25, 20, 1, 6, 1200]]

    for p in measure_params:
        iv = MeasureIV(p)
        try:
            iv.measureIV(hysteresis=False)
        except KeyboardInterrupt:
            pass
        finally:
            iv.exit_gracefully()
        del iv


if __name__ == '__main__':
    main()






