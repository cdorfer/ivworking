import numpy as np
from time import sleep, time
from Keithley2657 import Keithley2657
from DataHandler import DataHandler
from LiveMonitor import LiveMonitor



### CONFIG ###
diamond_name = 'None'
pad_number = 0

vmin = 0
vmax = 483
vstep = 30
settling_wait = 4
read_interval = 1 #second, one reading takes about 2s (acquires 10 data points internally and averages them)
readings = 2
iterations = 1
### end CONFIG ###

comments = 'vmin=' + str(vmin) + '\n vmax=' + str(vmax) + '\n vstep=' + str(vstep) \
            + '\n settling_wait=' + str(settling_wait) + '\n read_interval=' + str(read_interval) \
            + '\n readings=' + str(readings) + '\n interations=' + str(iterations) 


class MeasureIV(object):
    def __init__(self):
        self.cfg = 'config/keithley2657.cfg'
        self.hv = None
        #self.lm = LiveMonitor()
        self.dh = DataHandler()
        self.dh.createFile(diamond_name, pad_number, comments)
        self.done = False

 
    def start(self):
        self.hv = Keithley2657(self.cfg)
        self.hv.open_instrument()
        self.hv.init_keithley()
        
        ssteps = np.arange(vmin, vmax+1, vstep)
        self.hv.set_bias(0)
        self.hv.set_output(True)
        sleep(settling_wait)
    
        terminated = False
        for s, step in enumerate(ssteps[1::]): #don't do zero twice
            
            if terminated:
                break
            
            results = []
            for r in range(readings):
                print 'Reading ' + str(r) + '/' + str(readings) + ' of ' + str(s) + '/' + str(len(ssteps))
                data =  self.hv.read_iv()
                u = data['voltage'] #voltage in V
                curr = 1e9*data['current'] #current in nA
                compl = int(data['compliance']) #1 for compliance
                results.append([int(r), time(), float(u), float(curr), int(compl)])

                #check for compliance:
                complSum = np.sum(results, axis=0)[-1]
                if complSum > readings/2.0:
                    print 'Mimimi, compliance! Terminating, terminating!'
                    terminated = True
                    break
                sleep(read_interval)
                
            if not terminated:
                pres = np.mean(results, axis=0)
                print 'Scan point ' + str(s) + '/' + str(len(ssteps-1)) + ': U=' + str(round(pres[2],1)) + 'V, I=' + str(round(pres[3],4)) + 'nA'
                self.hv.set_bias(step)
                sleep(settling_wait)
            else:
                break


        self.hv.set_bias(0)
        self.hv.set_output(False)
        self.dh.closeFile()
        self.hv.close_instrument()
        self.done = True


    def exit_gracefully(self):
        if not self.done:
            self.hv.set_bias(0)
            self.dh.closeFile()
            self.hv.close_instrument()




if __name__ == '__main__':
    iv = MeasureIV()
    try:
        for i in range(iterations):
            iv.start()
    except KeyboardInterrupt:
        pass
    finally:
        iv.exit_gracefully()
        