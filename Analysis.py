import sys
import h5py
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt


def readFile(runnr):
    fname = 'data/run' + str(runnr) + '.hdf5'
    hdf = h5py.File(fname, 'r')
    ivdata = hdf['ivdata']
    nEntries = len(ivdata)
    
    print 'File name: ', fname
    print 'Diamond name:', ivdata.attrs['diamond_name']
    print 'Pad number:', ivdata.attrs['pad_number']
    print 'Comments:', ivdata.attrs['comments']
    
    
    v = []
    v_std = []
    c = []
    c_std = []

    for idx in range(nEntries):
        sp = ivdata[str(idx)]
        arr = np.array(sp)
        avgd = np.mean(arr, axis=0)
        std = np.std(arr, axis=0)

        v.append(avgd[2])
        v_std.append(std[2])
        c.append(avgd[3])
        c_std.append(std[3])

    return [v, v_std, c, c_std]
    hdf.close() 


def generate1DPlot(x, y, xlabel='', ylabel='', title='', save=False, savetitle='not_defined'):   
    fig, ax = plt.subplots(1)
    ax.set_xlabel(xlabel,fontsize=11)
    ax.set_ylabel(ylabel,fontsize=11)
    ax.set_title(title)
    ax.plot(x, y, marker='.', color='r', linewidth=0)
    if save:
        plt.savefig(savetitle + '.png', bbox_inches='tight', dpi=300)
        plt.savefig(savetitle + '.pdf', bbox_inches='tight')
    return fig



def main():
    #runs = [[4, 'k', 'Pad 1'],[5, 'r', 'Nothing'],[6, 'k', 'Pad 1'],[7, 'g', 'Pad 2'],[8, 'y', 'Pad 3'],[9, 'y', 'Pad 3']]
    '''
    runs = [[114, 'k', 'empty AC off'],
            [116, 'r', 'empty AC turing on'],
            [117, 'b', 'empty AC on'],
            [119, 'g', 'empty AC on'],
            [120, 'orange', 'empty AC on']
            ]
    '''
    runs = [[6, 'r', 'S83']]

    pl = generate1DPlot([], [], xlabel='Voltage [V]', ylabel='Current [pA]')
    ax = pl.gca()


    for r in runs:
        [v, v_std, c, c_std] = readFile(r[0])
        print v, v_std
        ax.errorbar(v, c, yerr=c_std, xerr=v_std, color=r[1], marker='+', linewidth=0.2, label=r[2])


    handles,labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, loc='upper left', title='Measurements')    
    pl.show()


    

    
if __name__ == '__main__':
    main()
